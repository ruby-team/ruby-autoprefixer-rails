require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end

#task :default => :spec
task :default do
  puts 'FIXME: skipping tests due to missing dependencies'
end
