ruby-autoprefixer-rails (10.1.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 10.1.0.0+dfsg
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Update minimum version of libjs-autoprefixer to 10.1

 -- Pirate Praveen <praveen@debian.org>  Fri, 25 Dec 2020 02:10:11 +0530

ruby-autoprefixer-rails (10.0.0.2+dfsg-1) experimental; urgency=medium

  * New upstream version 10.0.0.2+dfsg
  * Remove patches no longer required
  * Update minimum version of libjs-autoprefixer
  * Remove obsolete needs-recommends restriction in autopkgtest
  * Update minimum version of ruby-execjs to 2.7

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Sep 2020 16:18:18 +0530

ruby-autoprefixer-rails (9.8.5+dfsg-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set field Upstream-Contact in debian/copyright.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

  [ Pirate Praveen ]
  * New upstream version 8.6.5+dfsg

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Pirate Praveen ]
  * New upstream version 9.8.5+dfsg
  * Tighten dependency on libjs-autoprifixer

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Sep 2020 15:51:10 +0530

ruby-autoprefixer-rails (8.6.5+dfsg-3) unstable; urgency=medium

  * Use packaged version of libjs-autoprefixer
  * Remove git usage in gemspec
  * Add smoke test as autopkgtest

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 Mar 2019 13:28:47 +0530

ruby-autoprefixer-rails (8.6.5+dfsg-2) unstable; urgency=medium

  [ Xavier Guimard ]
  * Remove useless license
  * Remove useless lintian overrides
  * Add upstream/metadata
  * Switch build to rollup from webpack

  [ Pirate Praveen ]
  * Reupload to unstable
  * Remove babel step and add commonjs plugin to rollup.config.js

 -- Pirate Praveen <praveen@debian.org>  Thu, 07 Feb 2019 18:17:28 +0530

ruby-autoprefixer-rails (8.6.5+dfsg-1) experimental; urgency=medium

  * New upstream version 8.6.5+dfsg
  * Build vendor/autoprefixer.js using webpack and node-autoprefixer
    (Closes: #837463)
  * Exclude vendor/autoprefixer.js from orig source
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Move debian/watch to gemwatch.debian.net
  * Tighten dependency on node-autoprefixer
  * Use salsa.debian.org in Vcs-* fields

 -- Pirate Praveen <praveen@debian.org>  Sun, 06 Jan 2019 13:52:01 +0530

ruby-autoprefixer-rails (7.1.4.1-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Sat, 30 Sep 2017 17:15:29 +0530

ruby-autoprefixer-rails (6.7.6-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Wed, 28 Jun 2017 13:50:16 +0530

ruby-autoprefixer-rails (6.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Mon, 16 Jan 2017 20:24:59 +0530

ruby-autoprefixer-rails (6.4.0.3-2) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Use DH_RUBY = --gem-install

 -- Sruthi Chandran <srud@disroot.org>  Fri, 26 Aug 2016 10:43:31 +0530

ruby-autoprefixer-rails (6.3.3.1-1) unstable; urgency=medium

  * New upstream minor release

 -- Pirate Praveen <praveen@debian.org>  Sat, 05 Mar 2016 22:32:19 +0530

ruby-autoprefixer-rails (6.0.3-2) unstable; urgency=medium

  * Re-upload to unstable.
  * Check gemspec dependencies during build.

 -- Pirate Praveen <praveen@debian.org>  Sun, 20 Dec 2015 12:47:59 +0530

ruby-autoprefixer-rails (6.0.3-1) experimental; urgency=medium

  * New upstream release.
  * Add myself to uploaders.

 -- Pirate Praveen <praveen@debian.org>  Fri, 30 Oct 2015 02:45:02 +0530

ruby-autoprefixer-rails (5.2.1.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Pirate Praveen <praveen@debian.org>  Tue, 28 Jul 2015 17:04:16 +0530

ruby-autoprefixer-rails (5.1.8-2) unstable; urgency=medium

  * Team upload.
  * Install sutoprefixer.js in usr/share and set path

 -- Pirate Praveen <praveen@debian.org>  Sun, 12 Apr 2015 12:22:24 +0530

ruby-autoprefixer-rails (5.1.8-1) unstable; urgency=medium

  * Initial release

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 26 Mar 2015 12:04:22 -0300
